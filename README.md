# Palindrome Example

This is a simple example repo written to demonstrate how to write and release a [Crystal](https://crystal-lang.org/) shard. You can learn more by reading [the docs](https://crystal-lang.org/docs/guides/writing_shards.html).

## Installation

1. Add the dependency to your `shard.yml`:
```yaml
dependencies:
  palindrome-example:
    gitlab: crystal-examples/palindrome-example
```
2. Run `shards install`

## Usage

```crystal
require "palindrome-example"
```

This is just a dummy shard, but to make sure it has some code to format and a test to check, there is a method in there. I don't want to imply undue importance to it so I won't mention it by name here.

## Contributing

1. Fork it (<https://gitlab.com/crystal-examples/palindrome-example/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
