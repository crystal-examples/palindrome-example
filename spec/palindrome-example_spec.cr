require "./spec_helper"

describe Palindrome::Example do
  it "generates a random palindrome example" do
    palindrome = Palindrome::Example.generate
    palindrome[0].should eq(palindrome[-1])
  end
end
