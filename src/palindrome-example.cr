# Provides a namespace for the Palindrome Example
module Palindrome::Example
  VERSION = "0.1.0"

  # randomly generates an example palindrome
  def self.generate
    %w{racecar mom dad kayak madam}.sample(1)
  end
end
